package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;

public class ContextError {
    public final ParseTree node;
    public final String message;

    public ContextError(ParseTree node, String message) {
        this.node = node;
        this.message = message;
    }
}
