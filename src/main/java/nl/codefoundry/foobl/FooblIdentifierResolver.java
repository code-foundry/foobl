package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.List;

public class FooblIdentifierResolver {
    private final FooblParser.ProgramContext program;

    private IdentifierResolveResult identifierResolverResult = null;

    public FooblIdentifierResolver(FooblParser.ProgramContext program) {
        this.program = program;
    }

    public IdentifierResolveResult resolveIdentifiers() {
        if (identifierResolverResult == null) {
            var typeChecker = new IdentifierResolverFooblListener();
            ParseTreeWalker.DEFAULT.walk(typeChecker, program);
            identifierResolverResult = typeChecker.getResult();
        }
        return identifierResolverResult;
    }

    private static class IdentifierResolverFooblListener extends FooblBaseListener {
        private final ParseTreeProperty<ParseTree> identifierMap = new ParseTreeProperty<>();
        private final List<ContextError> errors = new ArrayList<>();
        private final IdentifierScopeStack identifierScopeStack = new IdentifierScopeStack();

        public IdentifierResolveResult getResult() {
            return new IdentifierResolveResult(identifierMap, errors);
        }

        @Override
        public void enterProgram(FooblParser.ProgramContext ctx) {
            identifierScopeStack.pushScope();
        }

        @Override
        public void enterDeclaration(FooblParser.DeclarationContext ctx) {
            identifierScopeStack.setDefiningNode(ctx.IDENTIFIER().getText(), ctx);
        }

        @Override
        public void enterBlockExpression(FooblParser.BlockExpressionContext ctx) {
            identifierScopeStack.pushScope();
        }

        @Override
        public void exitBlockExpression(FooblParser.BlockExpressionContext ctx) {
            identifierScopeStack.popScope();
        }

        @Override
        public void enterIdentifierExpression(FooblParser.IdentifierExpressionContext ctx) {
            var identifier = ctx.IDENTIFIER().getText();
            var definingNode = identifierScopeStack.getDefiningNode(identifier);

            if (definingNode == null) {
                errors.add(new ContextError(ctx, "No definition found for identifier '" + identifier + "'"));
            } else {
                identifierMap.put(ctx, definingNode);
            }
        }
    }
}
