package nl.codefoundry.foobl;

public enum FooblDataType {
    INTEGER,
    BOOLEAN,
}
