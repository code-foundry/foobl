package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.List;

public record IdentifierResolveResult(
        ParseTreeProperty<ParseTree> identifierMap,
        List<ContextError> errors
) { }
