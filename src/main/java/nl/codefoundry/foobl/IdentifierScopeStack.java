package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;

import java.util.HashMap;
import java.util.Map;

public class IdentifierScopeStack {
    private IdentifierScope currentScope;

    public void pushScope() {
        currentScope = new IdentifierScope(currentScope);
    }

    public void popScope() {
        assert(currentScope != null);
        currentScope = currentScope.parentScope;
    }

    public ParseTree getDefiningNode(String identifier) {
        assert(currentScope != null);
        return currentScope.getDefiningNode(identifier);
    }

    public void setDefiningNode(String identifier, ParseTree node) {
        assert(currentScope != null);
        currentScope.setDefiningNode(identifier, node);
    }

    private static class IdentifierScope {
        public final IdentifierScope parentScope;
        private final Map<String, ParseTree> identifierDefinitionMap = new HashMap<>();

        public IdentifierScope(IdentifierScope parentScope) {
            this.parentScope = parentScope;
        }

        public ParseTree getDefiningNode(String identifier) {
            if (identifierDefinitionMap.containsKey(identifier)) {
                return identifierDefinitionMap.get(identifier);
            }

            if (parentScope != null) {
                return parentScope.getDefiningNode(identifier);
            }

            return null;
        }

        public void setDefiningNode(String identifier, ParseTree node) {
            identifierDefinitionMap.put(identifier, node);
        }
    }
}
