package nl.codefoundry.foobl;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

public class FooblRun {
    public static void main(String[] args) throws IOException {
        var lexer = new FooblLexer(CharStreams.fromStream(System.in));
        var parser = new FooblParser(new CommonTokenStream(lexer));
        var fooblProgram = parser.program();

        var identifierResolver = new FooblIdentifierResolver(fooblProgram);
        var identifierResolveResult = identifierResolver.resolveIdentifiers();

        if (identifierResolveResult.errors().size() > 0) {
            reportContextErrors(identifierResolveResult.errors());
            return;
        }

        var typeChecker = new FooblTypeChecker(fooblProgram, identifierResolveResult.identifierMap());
        var typeCheckResult = typeChecker.typeCheck();

        if (typeCheckResult.errors().size() > 0) {
            reportContextErrors(typeCheckResult.errors());
            return;
        }

        var interpreter = new FooblInterpreter(fooblProgram, identifierResolveResult.identifierMap());
        var result = interpreter.evaluate();

        System.out.println("result = " + result);
    }

    private static void reportContextErrors(Iterable<ContextError> contextErrors) {
        for (var contextError : contextErrors) {
            reportContextError(contextError);
        }
    }

    private static void reportContextError(ContextError contextError) {
        System.err.println("[" + contextError.node.getText() + "] " + contextError.message);
    }
}
