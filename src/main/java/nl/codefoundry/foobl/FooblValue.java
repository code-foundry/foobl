package nl.codefoundry.foobl;

import java.util.Objects;

public abstract class FooblValue {
    public static final FooblValue TRUE = FooblValue.of(true);
    public static final FooblValue FALSE = FooblValue.of(true);

    protected FooblValue() {}

    public boolean getBoolean() {
        throw new RuntimeException("Value is not a boolean");
    }

    public long getInteger() {
        throw new RuntimeException("Value is not an integer");
    }

    @Override
    public String toString() {
        return valueAsString() + " [" + type() + "]";
    }

    public abstract FooblDataType type();

    public abstract String valueAsString();

    public static FooblValue of(long value) {
        return new FooblIntegerValue(value);
    }

    public static FooblValue of(boolean value) {
        return new FooblBooleanValue(value);
    }

    private static class FooblIntegerValue extends FooblValue {
        private final long value;

        private FooblIntegerValue(long value) {
            this.value = value;
        }

        @Override
        public long getInteger() {
            return value;
        }

        @Override
        public FooblDataType type() {
            return FooblDataType.INTEGER;
        }

        @Override
        public String valueAsString() {
            return String.valueOf(value);
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) return true;
            if (other == null || getClass() != other.getClass()) return false;
            return value == ((FooblIntegerValue) other).value;
        }

        @Override
        public int hashCode() {
            return Long.hashCode(value);
        }
    }

    private static class FooblBooleanValue extends FooblValue {
        private final boolean value;

        private FooblBooleanValue(boolean value) {
            this.value = value;
        }

        @Override
        public boolean getBoolean() {
            return value;
        }

        @Override
        public FooblDataType type() {
            return FooblDataType.BOOLEAN;
        }

        @Override
        public String valueAsString() {
            return String.valueOf(value);
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) return true;
            if (other == null || getClass() != other.getClass()) return false;
            return value == ((FooblBooleanValue) other).value;
        }

        @Override
        public int hashCode() {
            return Boolean.hashCode(value);
        }
    }
}
