package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.List;

public record TypeCheckResult(
        ParseTreeProperty<FooblDataType> nodeTypes,
        List<ContextError> errors
) { }
