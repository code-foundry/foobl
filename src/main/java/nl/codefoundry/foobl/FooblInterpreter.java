package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

public class FooblInterpreter {
    private final FooblParser.ProgramContext program;
    private final ParseTreeProperty<ParseTree> identifierMap;

    public FooblInterpreter(FooblParser.ProgramContext program, ParseTreeProperty<ParseTree> identifierMap) {
        this.program = program;
        this.identifierMap = identifierMap;
    }

    public FooblValue evaluate() {
        return new InterpretingFooblVisitor(identifierMap).visitProgram(program);
    }

    private static class InterpretingFooblVisitor extends FooblBaseVisitor<FooblValue> {
        private final ParseTreeProperty<ParseTree> identifierMap;
        private final ParseTreeProperty<FooblValue> declarationValueMap = new ParseTreeProperty<>();

        public InterpretingFooblVisitor(ParseTreeProperty<ParseTree> identifierMap) {
            this.identifierMap = identifierMap;
        }

        @Override
        public FooblValue visitMultiplicativeExpression(FooblParser.MultiplicativeExpressionContext ctx) {
            var left = visit(ctx.expression(0)).getInteger();
            var right = visit(ctx.expression(1)).getInteger();
            var operator = ctx.operator.getType();

            long result;
            if (operator == FooblLexer.MULTIPLY) {
                result = left * right;
            } else if (operator == FooblLexer.DIVIDE) {
                result = left / right;
            } else {
                result = left % right;
            }

            return FooblValue.of(result);
        }

        @Override
        public FooblValue visitAdditiveExpression(FooblParser.AdditiveExpressionContext ctx) {
            var left = visit(ctx.expression(0)).getInteger();
            var right = visit(ctx.expression(1)).getInteger();
            var operator = ctx.operator.getType();

            long result;
            if (operator == FooblLexer.PLUS) {
                result = left + right;
            } else {
                result = left - right;
            }

            return FooblValue.of(result);
        }

        @Override
        public FooblValue visitNotExpression(FooblParser.NotExpressionContext ctx) {
            return FooblValue.of(!visit(ctx.expression()).getBoolean());
        }

        @Override
        public FooblValue visitUnaryMinusExpression(FooblParser.UnaryMinusExpressionContext ctx) {
            return FooblValue.of(-visit(ctx.expression()).getInteger());
        }

        @Override
        public FooblValue visitRelationalExpression(FooblParser.RelationalExpressionContext ctx) {
            var left = visit(ctx.expression(0)).getInteger();
            var right = visit(ctx.expression(1)).getInteger();
            var operator = ctx.operator.getType();

            boolean result;
            if (operator == FooblLexer.LESS_THAN) {
                result = left < right;
            } else if (operator == FooblLexer.LESS_THAN_EQUALS) {
                result = left <= right;
            } else if (operator == FooblLexer.GREATER_THAN) {
                result = left > right;
            } else {
                result = left >= right;
            }

            return FooblValue.of(result);
        }

        @Override
        public FooblValue visitEqualityExpression(FooblParser.EqualityExpressionContext ctx) {
            var left = visit(ctx.expression(0));
            var right = visit(ctx.expression(1));
            var operator = ctx.operator.getType();

            boolean result;
            if (operator == FooblLexer.EQUALS) {
                result = left.equals(right);
            } else {
                result = !left.equals(right);
            }

            return FooblValue.of(result);
        }

        @Override
        public FooblValue visitLogicalAndExpression(FooblParser.LogicalAndExpressionContext ctx) {
            var left = visit(ctx.expression(0)).getBoolean();

            if (!left) {
                return FooblValue.FALSE; // Short-circuit evaluation
            }

            return visit(ctx.expression(1));
        }

        @Override
        public FooblValue visitLogicalOrExpression(FooblParser.LogicalOrExpressionContext ctx) {
            var left = visit(ctx.expression(0)).getBoolean();

            if (left) {
                return FooblValue.TRUE; // Short-circuit evaluation
            }

            return visit(ctx.expression(1));
        }

        @Override
        public FooblValue visitConditionalExpression(FooblParser.ConditionalExpressionContext ctx) {
            var condition = visit(ctx.expression(0)).getBoolean();

            return condition ? visit(ctx.expression(1)) : visit(ctx.expression(2));
        }

        @Override
        public FooblValue visitIdentifierExpression(FooblParser.IdentifierExpressionContext ctx) {
            var declaration = identifierMap.get(ctx);

            return declarationValueMap.get(declaration);
        }

        @Override
        public FooblValue visitLiteral(FooblParser.LiteralContext ctx) {
            if (ctx.BOOLEAN_LITERAL() != null) {
                return FooblValue.of(Boolean.parseBoolean(ctx.BOOLEAN_LITERAL().getText()));
            }

            return FooblValue.of(Long.parseLong(ctx.INTEGER().getText()));
        }

        @Override
        public FooblValue visitBlockExpression(FooblParser.BlockExpressionContext ctx) {
            for (var statement : ctx.block().blockStatement()) {
                var result = visit(statement);
                if (result != null) {
                    return result;
                }
            }

            return visit(ctx.block().expression());
        }

        @Override
        public FooblValue visitDeclaration(FooblParser.DeclarationContext ctx) {
            declarationValueMap.put(ctx, visit(ctx.expression()));

            return null;
        }

        @Override
        public FooblValue visitIfReturnStatement(FooblParser.IfReturnStatementContext ctx) {
            var condition = visit(ctx.expression(0)).getBoolean();

            return condition ? visit(ctx.expression(1)) : null;
        }

        @Override
        protected FooblValue aggregateResult(FooblValue aggregate, FooblValue nextResult) {
            return nextResult == null ? aggregate : nextResult;
        }
    }
}
