package nl.codefoundry.foobl;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FooblTypeChecker {
    private final FooblParser.ProgramContext program;
    private final ParseTreeProperty<ParseTree> identifierMap;

    private TypeCheckResult typeCheckResult = null;

    public FooblTypeChecker(FooblParser.ProgramContext program, ParseTreeProperty<ParseTree> identifierMap) {
        this.program = program;
        this.identifierMap = identifierMap;
    }

    public TypeCheckResult typeCheck() {
        if (typeCheckResult == null) {
            var typeChecker = new TypeCheckFooblListener(identifierMap);
            ParseTreeWalker.DEFAULT.walk(typeChecker, program);
            typeCheckResult = typeChecker.getResult();
        }
        return typeCheckResult;
    }

    private static class TypeCheckFooblListener extends FooblBaseListener {
        private final ParseTreeProperty<FooblDataType> nodeTypes = new ParseTreeProperty<>();
        private final List<ContextError> errors = new ArrayList<>();
        private final ParseTreeProperty<ParseTree> identifierMap;

        public TypeCheckFooblListener(ParseTreeProperty<ParseTree> identifierMap) {
            this.identifierMap = identifierMap;
        }

        @Override
        public void exitParenthesizedExpression(FooblParser.ParenthesizedExpressionContext ctx) {
            setNodeType(ctx, getNodeType(ctx.expression()));
        }

        @Override
        public void exitBlockExpression(FooblParser.BlockExpressionContext ctx) {
            setNodeType(ctx, getNodeType(ctx.block().expression()));
        }

        @Override
        public void exitLiteralExpression(FooblParser.LiteralExpressionContext ctx) {
            var literal = ctx.literal();
            var nodeType = literal.BOOLEAN_LITERAL() != null ? FooblDataType.BOOLEAN : FooblDataType.INTEGER;
            setNodeType(literal, nodeType);
            setNodeType(ctx, nodeType);
        }

        @Override
        public void exitIdentifierExpression(FooblParser.IdentifierExpressionContext ctx) {
            var referencedNode = identifierMap.get(ctx);

            setNodeType(ctx, getNodeType(referencedNode));
        }

        @Override
        public void exitConditionalExpression(FooblParser.ConditionalExpressionContext ctx) {
            var conditionExpression = ctx.expression(0);
            var ifBranchExpression = ctx.expression(1);
            var elseBranchExpression = ctx.expression(2);
            var conditionType = getNodeType(conditionExpression);
            var ifBranchType = getNodeType(ifBranchExpression);
            var elseBranchType = getNodeType(elseBranchExpression);

            if (conditionType != FooblDataType.BOOLEAN) {
                errors.add(new ContextError(
                        conditionExpression,
                        "expression should evaluate to " + FooblDataType.BOOLEAN + ", got " + conditionType + " instead"
                ));
            }

            if (ifBranchType != elseBranchType) {
                errors.add(new ContextError(
                        elseBranchExpression,
                        "else-branch result " + elseBranchType + " does not match if-branch result " + ifBranchType
                ));
            }

            setNodeType(ctx, ifBranchType);
        }

        @Override
        public void exitLogicalOrExpression(FooblParser.LogicalOrExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.BOOLEAN_BOOLEAN);
            setNodeType(ctx, FooblDataType.BOOLEAN);
        }

        @Override
        public void exitLogicalAndExpression(FooblParser.LogicalAndExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.BOOLEAN_BOOLEAN);
            setNodeType(ctx, FooblDataType.BOOLEAN);
        }

        @Override
        public void exitEqualityExpression(FooblParser.EqualityExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.BOOLEAN_BOOLEAN, OperandTypePair.INTEGER_INTEGER);
            setNodeType(ctx, FooblDataType.BOOLEAN);
        }

        @Override
        public void exitRelationalExpression(FooblParser.RelationalExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.INTEGER_INTEGER);
            setNodeType(ctx, FooblDataType.BOOLEAN);
        }

        @Override
        public void exitUnaryMinusExpression(FooblParser.UnaryMinusExpressionContext ctx) {
            var operand = ctx.expression();
            var operandType = getNodeType(operand);
            if (operandType != FooblDataType.INTEGER) {
                errors.add(new ContextError(
                        operand,
                        "expected: " + FooblDataType.INTEGER + ", got: " + operandType
                ));
            }
            setNodeType(ctx, FooblDataType.INTEGER);
        }

        @Override
        public void exitNotExpression(FooblParser.NotExpressionContext ctx) {
            var operand = ctx.expression();
            var operandType = getNodeType(operand);
            if (operandType != FooblDataType.BOOLEAN) {
                errors.add(new ContextError(
                        operand,
                        "expected: " + FooblDataType.BOOLEAN + ", got: " + operandType
                ));
            }
            setNodeType(ctx, FooblDataType.BOOLEAN);
        }

        @Override
        public void exitAdditiveExpression(FooblParser.AdditiveExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.INTEGER_INTEGER);
            setNodeType(ctx, FooblDataType.INTEGER);
        }

        @Override
        public void exitMultiplicativeExpression(FooblParser.MultiplicativeExpressionContext ctx) {
            var leftOperand = ctx.expression(0);
            var rightOperand = ctx.expression(1);
            checkBinaryOperator(leftOperand, rightOperand, OperandTypePair.INTEGER_INTEGER);
            setNodeType(ctx, FooblDataType.INTEGER);
        }

        @Override
        public void exitBlock(FooblParser.BlockContext ctx) {
            var blockReturnType = getNodeType(ctx.expression());
            setNodeType(ctx, blockReturnType);

            for (var statement : ctx.blockStatement()) {
                if (statement instanceof FooblParser.IfReturnStatementContext) {
                    var returnExpression = ((FooblParser.IfReturnStatementContext) statement).expression(1);
                    var returnExpressionType = getNodeType(returnExpression);

                    if (returnExpressionType != blockReturnType) {
                        errors.add(new ContextError(
                                returnExpression,
                                "expression evaluates to " + returnExpressionType + " and does not match the " +
                                        blockReturnType + " return type of the block expression"
                        ));
                    }
                }
            }
        }

        @Override
        public void exitIfReturnStatement(FooblParser.IfReturnStatementContext ctx) {
            var conditionExpression = ctx.expression(0);
            var returnExpression = ctx.expression(1);
            var conditionType = getNodeType(conditionExpression);

            if (conditionType != FooblDataType.BOOLEAN) {
                errors.add(new ContextError(
                        conditionExpression,
                        "expression should evaluate to " + FooblDataType.BOOLEAN + ", got " + conditionType + " instead"
                ));
            }

            setNodeType(ctx, getNodeType(returnExpression));
        }

        @Override
        public void exitDeclaration(FooblParser.DeclarationContext ctx) {
            setNodeType(ctx, getNodeType(ctx.expression()));
        }

        @Override
        public void exitProgram(FooblParser.ProgramContext ctx) {
            setNodeType(ctx, getNodeType(ctx.expression()));
        }

        public TypeCheckResult getResult() {
            return new TypeCheckResult(nodeTypes, errors);
        }

        private FooblDataType getNodeType(ParseTree node) {
            return nodeTypes.get(node);
        }

        private void setNodeType(ParseTree node, FooblDataType type) {
            nodeTypes.put(node, type);
        }

        private void checkBinaryOperator(ParseTree leftOperand, ParseTree rightOperand, OperandTypePair... allowedOperandPairs) {
            var leftOperandType = getNodeType(leftOperand);
            var rightOperandType = getNodeType(rightOperand);

            var hasMatchingOperandPair = Arrays.stream(allowedOperandPairs).anyMatch((allowedOperandPair) ->
                    allowedOperandPair.left == leftOperandType && allowedOperandPair.right == rightOperandType
            );

            if (!hasMatchingOperandPair && allowedOperandPairs.length > 0) {
                if (leftOperandType != allowedOperandPairs[0].left) {
                    errors.add(new ContextError(
                            leftOperand,
                            "expected: " + allowedOperandPairs[0].left + ", got: " + leftOperandType
                    ));
                }

                if (rightOperandType != allowedOperandPairs[0].right) {
                    errors.add(new ContextError(
                            rightOperand,
                            "expected: " + allowedOperandPairs[0].right + ", got: " + rightOperandType
                    ));
                }
            }
        }
    }

    private record OperandTypePair(FooblDataType left, FooblDataType right) {
        public static final OperandTypePair INTEGER_INTEGER = of(FooblDataType.INTEGER, FooblDataType.INTEGER);
        public static final OperandTypePair BOOLEAN_BOOLEAN = of(FooblDataType.BOOLEAN, FooblDataType.BOOLEAN);

        public static OperandTypePair of(FooblDataType left, FooblDataType right) {
            return new OperandTypePair(left, right);
        }
    }
}
