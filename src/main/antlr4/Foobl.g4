grammar Foobl;

@header {
package nl.codefoundry.foobl;
}

program
    : statement* expression EOF
    ;

statement
    : declaration ';'
    ;

declaration
    : DEF IDENTIFIER '=' expression
    ;

expression
    : expression operator=('*' | '/' | '%') expression            # MultiplicativeExpression
    | expression operator=('+' | '-') expression                  # AdditiveExpression
    | '!' expression                                              # NotExpression
    | '-' expression                                              # UnaryMinusExpression
    | expression operator=('<' | '>' | '<=' | '>=') expression    # RelationalExpression
    | expression operator=('==' | '!=') expression                # EqualityExpression
    | expression '&&' expression                                  # LogicalAndExpression
    | expression '||' expression                                  # LogicalOrExpression
    | IF expression THEN expression ELSE expression               # ConditionalExpression
    | IDENTIFIER                                                  # IdentifierExpression
    | literal                                                     # LiteralExpression
    | '{' block '}'                                               # BlockExpression
    | '(' expression ')'                                          # ParenthesizedExpression
    ;

block
    : blockStatement* RETURN? expression
    ;

blockStatement
    : statement                           # RegularStatement
    | IF expression RETURN expression ';' # IfReturnStatement
    ;

literal
    : INTEGER
    | BOOLEAN_LITERAL
    ;

ASSIGN: '=';
MULTIPLY: '*';
DIVIDE: '/';
MODULUS: '%';
PLUS: '+';
MINUS: '-';
NOT: '!';
COLON: ':';
SEMI_COLON: ';';
OPEN_PAREN: '(';
CLOSE_PAREN: ')';
OPEN_BRACE: '{';
CLOSE_BRACE: '}';
LESS_THAN: '<';
GREATER_THAN: '>';
LESS_THAN_EQUALS: '<=';
GREATER_THAN_EQUALS: '>=';
EQUALS: '==';
NOT_EQUALS: '!=';
AND: '&&';
OR: '||';

DEF: 'def';
IF: 'if';
THEN: 'then';
ELSE: 'else';
RETURN: 'return';

BOOLEAN_LITERAL
    : 'true'
    | 'false'
    ;

IDENTIFIER
    : IDENTIFIER_START IDENTIFIER_PART*
    ;

fragment IDENTIFIER_START
    : [a-zA-Z_]
    ;

fragment IDENTIFIER_PART
    : IDENTIFIER_START
    | [0-9]
    ;

INTEGER
    : [0-9]+
    ;

LINE_COMMENT: '//' .*? '\r'? '\n' -> skip;
COMMENT: '/*' .*? '*/' -> skip;
WHITE_SPACE: [\t\n\r\u000B\u000C\u0020\u00A0]+ -> skip;
